# Encumbrance Variant (5e)

This module modifies how the encumbrance bar in the actor sheet is displayed to distinguish the different levels of encumbrance when using the variant rules in PHB p175.

![example](preview.gif)

### Current Issues:
- I wanted to include the Powerful Build racial feature into this mod as it effects how encumbrance is displayed. Unfortunately I could not do this without extending the base Actor5eSheet class - as a result I have removed this functionality and will wait until it is added in the base 5e system.

### FVTT Dependencies:
- Requires v0.2.8 of FVTT

### Installation Instructions

To install a module, follow these instructions:

1. [Download the zip file](https://gitlab.com/hooking/foundry-vtt---encumbrance-variant-5e/raw/master/encumbrancevariant5e.zip) included in the module directory. If one is not provided, the module is still in early development.
2. Extract the included folder to `public/modules` in your Foundry Virtual Tabletop installation folder.
3. Restart Foundry Virtual Tabletop.  

### Feedback

If you have any suggestions or feedback, please contact me on discord (hooking#0492)