class EncumbranceVariant5e extends Application {
  constructor(app) {
      super(app);
  }

  init () {

    Hooks.on(`renderActorSheet5eCharacter`, (app, html, data) => { this.addVariantEncumberanceBar(app, html, data); });    

  }

  addVariantEncumberanceBar(app, html, data) {

    let noEncumbranceBarHTML = `<span class="variant-encumbrance-bar" style="width:33.0972%" title="No Encumberance"></span>`;
    let lightEncumbranceBarHTML = `<span class="variant-encumbrance-bar" style="width:33.0972%" title="Lightly Encumbered (-10 Speed)"></span>`;
    let heavyEncumbranceBarHTML = `<span class="variant-encumbrance-bar" style="width:33.0972%" title="Heavily Encumbered (-20 Speed, disadvantage on STR, DEX and CON checks/rolls/saves)"></span>`;

    html.find('.encumbrance').prepend(heavyEncumbranceBarHTML);
    html.find('.encumbrance').prepend(lightEncumbranceBarHTML);
    html.find('.encumbrance').prepend(noEncumbranceBarHTML);
  }

}

let encumbranceVariant = new EncumbranceVariant5e();
encumbranceVariant.init();